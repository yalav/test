package com.company;

public class testClass {
    private int test;

    public testClass(int test) {
        this.test = test;
    }

    public testClass() {
        System.out.println("in constructor");
    }

    public int getTest() {
        return test;
    }

    public void setTest(int test) {
        this.test = test;
    }
}
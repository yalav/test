package com.company;

import java.time.LocalDate;
import java.time.Month;

public class Univer {
    static Student[] arrayStudy;

    static {

        arrayStudy = new Student[3];

        arrayStudy[0] = new Student(1,
                "Ivanov A A",
                LocalDate.of(1991, Month.NOVEMBER, 30),
                "Minsk 11",
                "111-11-11",
                "IT",
                1,
                11);
        arrayStudy[1] = new Student(2,
                "Petrov P P",
                LocalDate.of(1988, Month.APRIL, 15),
                "Minsk 22",
                "222-22-22",
                "FPM",
                2,
                22);

        arrayStudy[2] = new Student(3,
                "Sidorov S S",
                LocalDate.of(1985, Month.DECEMBER, 3),
                "Minsk 33",
                "333-33-33",
                "TT",
                1,
                11);

    }

    static Student[] arrayGetStFak(String fakultet){

        Student[] res = new Student[3];
        int i = 0;

        for (Student stud: arrayStudy) {
            if (stud.fakultet == fakultet){
                res[i] = stud;
                i++;
            }

        }

        return res;
    }

}

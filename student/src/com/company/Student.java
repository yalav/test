package com.company;

import java.time.LocalDate;

public class Student {
    int id;
    String fio;
    LocalDate date;
    String address;
    String phone;
    String fakultet;
    int kurs;
    int group;

    Student(int id, String fio, LocalDate date, String address, String phone, String fakultet, int kurs, int group) {
        this.id = id;
        this.fio = fio;
        this.date = date;
        this.address = address;
        this.phone = phone;
        this.fakultet = fakultet;
        this.kurs = kurs;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", date=" + date +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", fakultet='" + fakultet + '\'' +
                ", kurs=" + kurs +
                ", group=" + group +
                '}';
    }

}



package com.company;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        // write your code here
        String[] array1;
        array1 = new String[args.length];
        String[] array2;
        array2 = new String[args.length];
        String[] array3;
        array3 = new String[args.length];

        int j1 = 0;
        int j2 = 0;
        int j3 = 0;

        for (int i = 0; i < args.length; i++) {

            String[] s = args[i].split("_");

            if (Integer.parseInt(s[0]) > 500) {
                array1[j1] = args[i].toLowerCase();
                j1 = j1++;
            }
            if (Integer.parseInt(s[0]) < 500) {
                array2[j2] = args[i].toLowerCase();
                j2 = j2++;
            }
            if (Integer.parseInt(s[0]) == 500) {
                array3[j3] = args[i];
                j3 = j3++;
            }
        }

        for (String s : array1) {
            if (s != null)
            System.out.println("array1: " + s);
        }
        for (String s : array2) {
            if (s != null)
            System.out.println("array2: " + s);
        }
        for (String s : array3) {
            if (s != null)
            System.out.println("array3: " + new StringBuffer(s).reverse());
        }
    }
}

package com.company;

import java.util.Objects;

public class Tree implements Comparable{
    private int h;
    private String name;

    Tree(int h, String name){
        this.h = h;
        this.name = name;
    }

    Tree(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "h=" + h +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o==null)
            return false;

        if (o.getClass() != this.getClass() )
            return false;

        Tree t = (Tree)o;
        return this.h == t.h;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Tree t = (Tree)o;
        if (this.h < t.h){
            return -1;
        }
        else if (this.h > t.h){
            return 1;
        }
        else
            return 0;
    }
}